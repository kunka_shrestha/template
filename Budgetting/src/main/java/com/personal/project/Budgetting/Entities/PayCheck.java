package com.personal.project.Budgetting.Entities;

import java.sql.Date;

public class PayCheck {
	private int Id;
	private Date payCheckDate;
	private double Amount;
	private int SavingsID;
	private int ExpensesID;

	public PayCheck() {

	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Date getPayCheckDate() {
		return payCheckDate;
	}

	public void setPayCheckDate(Date payCheckDate) {
		this.payCheckDate = payCheckDate;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public int getSavingsID() {
		return SavingsID;
	}

	public void setSavingsID(int savingsID) {
		SavingsID = savingsID;
	}

	public int getExpensesID() {
		return ExpensesID;
	}

	public void setExpensesID(int expensesID) {
		ExpensesID = expensesID;
	}

}
