package com.personal.project.Budgetting.Entities;

public class TypeOfExpenses {

	private int Id;
	private String expenseName;
	
	
	public TypeOfExpenses() {
		
	}
	
	public int getId() {
		return Id;
	}
	
	public void setId(int id) {
		Id = id;
	}
	
	public String getExpenseName() {
		return expenseName;
	}
	
	public void setExpenseName(String expenseName) {
		this.expenseName = expenseName;
	}

}
