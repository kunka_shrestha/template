package com.personal.project.Budgetting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SavingsCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SavingsCalculatorApplication.class, args);
	}

}
